// ==UserScript==
// @name        Twitch Chat Buttons
// @namespace   https://www.twitch.tv/
// @description Permet d'ajouter des boutons dans le chat Twitch pour spammer des messages prédéfinis.
// @match       https://www.twitch.tv/*
// @match       https://www.twitch.tv/popout/*/chat*
// @version     1.4.1
// @grant       none
// @author      Desmu
// @homepageURL https://gitlab.com/Desmu/twitch-chat-buttons
// @updateURL   https://gitlab.com/Desmu/twitch-chat-buttons/-/raw/main/twitch-chat-buttons.user.js
// @downloadURL https://gitlab.com/Desmu/twitch-chat-buttons/-/raw/main/twitch-chat-buttons.user.js
// ==/UserScript==

function minifyString (str) {
    return str.trim().replace(/\r?\n|\r/gu, " ").replace(/ {2}/gu, "");
}

function wysiwygDestroyer () {
    if (document.querySelector("[data-a-target='chat-input']").tagName === "DIV") {
        document.querySelector("[data-a-target='chat-input-text']").innerHTML = "<span data-slate-string='true'></span>";
        document.querySelector("[data-a-target='chat-input']").focus();
        document.querySelector("[data-a-target='chat-input']").blur();
        document.querySelector("[data-a-target='chat-input']").focus();
    }
    document.querySelectorAll(".no-wysiwyg").forEach((noWysiwygTag) => {
        noWysiwygTag.remove();
    });
}

function sendMessage (message, sendable = true) {
    if (document.querySelector("[data-a-target='chat-input']").tagName === "DIV") {
        document.querySelector("[data-a-target='chat-input']").focus();
        if (sendable === true) {
            document.querySelector("[data-a-target='chat-input']").dispatchEvent(new InputEvent("beforeinput", {"bubbles": true, "inputType": "deleteEntireSoftLine"}));
            document.querySelector("[data-a-target='chat-input']").dispatchEvent(new InputEvent("beforeinput", {"bubbles": true, "inputType": "insertText", "data": message}));
            setTimeout(() => {
                document.querySelector("[data-a-target='chat-send-button']").click();
                document.querySelector("[data-a-target='chat-input']").blur();
            }, 100);
        }
    } else if (document.querySelector("[data-a-target='chat-input']").tagName === "TEXTAREA") {
        document.querySelector("[data-a-target='chat-input']").focus();
        Object.getOwnPropertyDescriptor(window.HTMLTextAreaElement.prototype, "value").set.call(document.querySelector("[data-a-target='chat-input']"), message);
        if (sendable === true) {
            document.querySelector("[data-a-target='chat-input']").dispatchEvent(new Event("input", {"bubbles": true}));
            document.querySelector("[data-a-target='chat-send-button']").click();
            document.querySelector("[data-a-target='chat-input']").blur();
        }
    }
}

function downloadFile (fileContent, fileName) {
    const dataStr = `data:text/json;charset=utf-8,${encodeURIComponent(fileContent)}`;
    const downloadAnchorNode = document.createElement("a");
    downloadAnchorNode.setAttribute("href", dataStr);
    downloadAnchorNode.setAttribute("download", fileName);
    document.body.appendChild(downloadAnchorNode);
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
}

function uploadFile (fileTag, callback) {
    const [file] = fileTag.files;
    if (file) {
        const reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = (evt) => {
            callback(evt.target.result);
        };
    }
}

function getStreamerName () {
    const urlParams = window.location.href.split("/");
    if (urlParams[urlParams.length - 1] === "") {
        urlParams.pop();
    }
    let streamerName = "";
    if (window.location.href.indexOf("/popout/") === -1) {
        streamerName = urlParams[urlParams.length - 1];
    } else {
        streamerName = urlParams[urlParams.length - 2];
    }
    return streamerName;
}

function twitchChatButtonsCreateStyle () {
    const styleTag = document.createElement("style");
    styleTag.innerText = minifyString(`
        .twitch-chat-button, .twitch-chat-button-field { display: inline-block; margin: 5px; padding: 5px; }
        .twitch-chat-button { background-color: var(--color-background-button-primary-default); border-radius: var(--border-radius-medium); color: var(--color-text-button-primary); cursor: pointer; font-weight: var(--font-weight-semibold); }
        .twitch-chat-button:hover, .twitch-chat-button-focused { background-color: var(--color-background-button-primary-hover); }
        .twitch-chat-button-field { background-color: var(--color-background-input); border: var(--border-width-input) solid var(--color-border-input); border-radius: 0.4rem; color: var(--color-text-input); }
        .twitch-chat-button-delete { background-color: var(--color-background-button-destructive-default); border-radius: var(--border-radius-medium); color: var(--color-text-button-primary); font-weight: var(--font-weight-semibold); margin: 0 -5px 0 5px; padding: 5px; }
        .twitch-chat-button-delete:hover { background-color: var(--color-background-button-destructive-hover); }
        .twitch-chat-buttons-hidden { display: none; }
        .twitch-chat-button-emote { vertical-align: middle; }
        hr.twitch-chat-buttons-hr { border: 1px solid #808080; }
    `);
    document.querySelector("head").append(styleTag);
}

function twitchChatButtonsCreateInterface () {
    const twitchChatGlobalButton = document.createElement("button");
    twitchChatGlobalButton.classList.add("twitch-chat-button", "twitch-chat-button-icon");
    twitchChatGlobalButton.setAttribute("type", "button");
    twitchChatGlobalButton.innerText = "Boutons";
    document.querySelector(".chat-input__buttons-container > div:last-child").append(twitchChatGlobalButton);

    const div = document.createElement("div");
    div.classList.add("twitch-chat-buttons");
    const divHTML = minifyString(`
        <div class='twitch-chat-buttons-div-buttons twitch-chat-buttons-hidden'>
            <hr class='twitch-chat-buttons-hr'>
        </div>
        <div class='twitch-chat-buttons-div-groups twitch-chat-buttons-hidden'>
            <hr class='twitch-chat-buttons-hr'>
        </div>
        <div class='twitch-chat-buttons-div-settings twitch-chat-buttons-hidden'>
            <hr class='twitch-chat-buttons-hr'>
            <button type='button' class='twitch-chat-button twitch-chat-button-add'>Ajouter</button>
            <button type='button' class='twitch-chat-button twitch-chat-button-del'>Supprimer</button>
            <button type='button' class='twitch-chat-button twitch-chat-button-import-export'>Importer/Exporter</button>
            <button type='button' class='twitch-chat-button no-wysiwyg'>NO WYSIWYG</button>
            <div class='twitch-chat-buttons-div-settings-add twitch-chat-buttons-hidden'>
                <input type='text' placeholder='Titre du bouton' class='twitch-chat-button-field twitch-chat-button-field-title'>
                <input type='text' placeholder='Texte à envoyer' class='twitch-chat-button-field twitch-chat-button-field-command'>
                <input type='text' placeholder='Groupe du bouton (facultatif)' class='twitch-chat-button-field twitch-chat-button-field-group'>
                <label for='twitch-chat-button-field-sendable' class='twitch-chat-button twitch-chat-button-focused'>Envoyer le texte au clic <input type='checkbox' id='twitch-chat-button-field-sendable' class='twitch-chat-button-field-sendable' checked></label>
                <button type='button' class='twitch-chat-button twitch-chat-button-add-button'>Ajouter un bouton</button>
            </div>
            <div class='twitch-chat-buttons-div-settings-import-export twitch-chat-buttons-hidden'>
                <button type='button' class='twitch-chat-button twitch-chat-button-export-these'>Exporter les boutons de ce chat</button>
                <label for='twitch-chat-button-import-these' class='twitch-chat-button'>Importer les boutons d'un seul chat<input type='file' accept='application/json' id='twitch-chat-button-import-these' class='twitch-chat-button-import-these twitch-chat-buttons-hidden'></label>
                <button type='button' class='twitch-chat-button twitch-chat-button-export-all'>Exporter tous les boutons</button>
                <label for='twitch-chat-button-import-all' class='twitch-chat-button'>Importer tous les boutons<input type='file' accept='application/json' id='twitch-chat-button-import-all' class='twitch-chat-button-import-all twitch-chat-buttons-hidden'></label>
            </div>
        </div>
    `);
    div.innerHTML = divHTML;
    document.querySelector(".chat-input").append(div);
    JSON.parse(localStorage.buttons).forEach((btn) => {
        if (btn.user === getStreamerName()) {
            twitchChatButtonsShowButton(btn);
        }
    });
}

function twitchChatButtonsToggle () {
    document.querySelectorAll(".twitch-chat-buttons-div-buttons, .twitch-chat-buttons-div-groups, .twitch-chat-buttons-div-settings").forEach((tag) => {
        tag.classList.toggle("twitch-chat-buttons-hidden");
    });
}

function twitchChatButtonsShowButton (btn) {
    twitchChatButtonsShowGroupButton(btn.group);

    const chatBtnDelete = document.createElement("button");
    chatBtnDelete.setAttribute("type", "button");
    chatBtnDelete.classList.add("twitch-chat-button-delete");
    chatBtnDelete.classList.add("twitch-chat-buttons-hidden");
    chatBtnDelete.innerText = "X";
    document.querySelector(".twitch-chat-buttons-div-buttons").append(chatBtnDelete);
    chatBtnDelete.addEventListener("click", () => {
        twitchChatButtonsDelButton(chatBtnDelete.nextSibling);
        chatBtnDelete.remove();
    });

    const chatBtn = document.createElement("button");
    chatBtn.setAttribute("type", "button");
    chatBtn.setAttribute("data-twitch-chat-buttons-command", btn.command);
    chatBtn.setAttribute("data-twitch-chat-buttons-group", btn.group);
    chatBtn.setAttribute("data-twitch-chat-buttons-sendable", btn.sendable);
    chatBtn.classList.add("twitch-chat-button");
    if ((btn.group !== "") && (!document.querySelector(`.twitch-chat-button-group[data-twitch-chat-buttons-group='${btn.group}']`).classList.contains("twitch-chat-button-focused"))) {
        chatBtn.classList.add("twitch-chat-buttons-hidden");
    }
    let textToShow = btn.title;
    Object.entries(JSON.parse(localStorage["twilight.emote_picker_history"])).forEach(([emoteId, emoteData]) => {
        if (btn.title.indexOf(emoteData.emote.token) !== -1) {
            textToShow = textToShow.replace(emoteData.emote.token, `<img src='https://static-cdn.jtvnw.net/emoticons/v2/${emoteId}/default/light/1.0' alt='${btn.title} class="twitch-chat-button-emote"'>`);
        }
    });
    chatBtn.innerHTML = textToShow;
    document.querySelector(".twitch-chat-buttons-div-buttons").append(chatBtn);
    chatBtn.addEventListener("click", () => {
        sendMessage(chatBtn.getAttribute("data-twitch-chat-buttons-command"), JSON.parse(chatBtn.getAttribute("data-twitch-chat-buttons-sendable")));
    });
}

function twitchChatButtonsShowGroup (group) {
    document.querySelector(".twitch-chat-button-del").classList.remove("twitch-chat-button-focused");
    document.querySelector(`.twitch-chat-button-group[data-twitch-chat-buttons-group='${group}']`).classList.toggle("twitch-chat-button-focused");
    if (document.querySelector(`.twitch-chat-button-group[data-twitch-chat-buttons-group='${group}']`).classList.contains("twitch-chat-button-focused")) {
        document.querySelectorAll(`.twitch-chat-button-group:not([data-twitch-chat-buttons-group='${group}'])`).forEach((tag) => {
            tag.classList.remove("twitch-chat-button-focused");
        });
        document.querySelectorAll(".twitch-chat-buttons-div-buttons > button").forEach((btn) => {
            if (btn.getAttribute("data-twitch-chat-buttons-group") === group) {
                btn.classList.remove("twitch-chat-buttons-hidden");
            } else {
                btn.classList.add("twitch-chat-buttons-hidden");
            }
        });
    } else {
        document.querySelectorAll(".twitch-chat-buttons-div-buttons > button").forEach((btn) => {
            if (btn.getAttribute("data-twitch-chat-buttons-group") === "") {
                btn.classList.remove("twitch-chat-buttons-hidden");
            } else {
                btn.classList.add("twitch-chat-buttons-hidden");
            }
        });
    }
}

function twitchChatButtonsShowGroupButton (group) {
    if ((group !== "") && (!document.querySelector(`.twitch-chat-button-group[data-twitch-chat-buttons-group='${group}']`))) {
        const chatBtnGroup = document.createElement("button");
        chatBtnGroup.setAttribute("type", "button");
        chatBtnGroup.setAttribute("data-twitch-chat-buttons-group", group);
        chatBtnGroup.classList.add("twitch-chat-button", "twitch-chat-button-group");
        chatBtnGroup.innerText = group;
        document.querySelector(".twitch-chat-buttons-div-groups").append(chatBtnGroup);
        chatBtnGroup.addEventListener("click", () => {
            twitchChatButtonsShowGroup(group);
        });
    }
}

function twitchChatButtonsAddButton () {
    if ((document.querySelector(".twitch-chat-button-field-title").value !== "") && (document.querySelector(".twitch-chat-button-field-command").value)) {
        const localButtons = JSON.parse(localStorage.buttons);
        const newButton = {
            "title": document.querySelector(".twitch-chat-button-field-title").value,
            "command": document.querySelector(".twitch-chat-button-field-command").value,
            "group": document.querySelector(".twitch-chat-button-field-group").value,
            "sendable": document.querySelector(".twitch-chat-button-field-sendable").checked,
            "user": getStreamerName()
        };
        localButtons.push(newButton);
        localStorage.buttons = JSON.stringify(localButtons);
        twitchChatButtonsShowButton(newButton);
        document.querySelector(".twitch-chat-button-field-title").value = "";
        document.querySelector(".twitch-chat-button-field-command").value = "";
        document.querySelector(".twitch-chat-button-field-group").value = "";
    }
}

function twitchChatButtonsDelButton (cmdBtn) {
    const group = cmdBtn.getAttribute("data-twitch-chat-buttons-group");
    const localButtons = JSON.parse(localStorage.buttons).filter((btn) => !(btn.user === getStreamerName() && btn.command === cmdBtn.getAttribute("data-twitch-chat-buttons-command")));
    localStorage.buttons = JSON.stringify(localButtons);
    cmdBtn.remove();
    if ((group !== "") && (document.querySelectorAll(`.twitch-chat-buttons-div-buttons > button[data-twitch-chat-buttons-group='${group}']`).length === 0)) {
        document.querySelector(`.twitch-chat-button-group[data-twitch-chat-buttons-group='${group}']`).remove();
    }
}

function twitchChatButtonsImportAllButtons (fileContent) {
    document.querySelector(".twitch-chat-buttons-div-buttons").innerHTML = "<hr class='twitch-chat-buttons-hr'>";
    localStorage.buttons = fileContent;
    JSON.parse(localStorage.buttons).forEach((btn) => {
        if (btn.user === getStreamerName()) {
            twitchChatButtonsShowButton(btn);
        }
    });
}

function twitchChatButtonsImportTheseButtons (fileContent) {
    const localButtons = JSON.parse(localStorage.buttons).filter((btn) => btn.user !== getStreamerName());
    JSON.parse(fileContent).forEach((newButton) => {
        localButtons.push(newButton);
    });
    localStorage.buttons = JSON.stringify(localButtons);
    document.querySelector(".twitch-chat-buttons-div-buttons").innerHTML = "<hr class='twitch-chat-buttons-hr'>";
    JSON.parse(localStorage.buttons).forEach((btn) => {
        if (btn.user === getStreamerName()) {
            twitchChatButtonsShowButton(btn);
        }
    });
}

function twitchChatButtonsAddButtonInterface () {
    if (document.querySelector(".twitch-chat-button-del").classList.contains("twitch-chat-button-focused")) {
        twitchChatButtonsDelButtonInterface();
    }
    if (document.querySelector(".twitch-chat-buttons-div-settings-import-export").classList.contains("twitch-chat-button-focused")) {
        twitchChatButtonsImportExportButtonInterface();
    }
    document.querySelector(".twitch-chat-button-add").classList.toggle("twitch-chat-button-focused");
    document.querySelector(".twitch-chat-buttons-div-settings-add").classList.toggle("twitch-chat-buttons-hidden");
}

function twitchChatButtonsDelButtonInterface () {
    if (document.querySelector(".twitch-chat-button-add").classList.contains("twitch-chat-button-focused")) {
        twitchChatButtonsAddButtonInterface();
    }
    if (document.querySelector(".twitch-chat-buttons-div-settings-import-export").classList.contains("twitch-chat-button-focused")) {
        twitchChatButtonsImportExportButtonInterface();
    }
    document.querySelector(".twitch-chat-button-del").classList.toggle("twitch-chat-button-focused");
    document.querySelectorAll(".twitch-chat-button-delete").forEach((deleteTag) => {
        if (document.querySelector(".twitch-chat-button-del").classList.contains("twitch-chat-button-focused")) {
            if (deleteTag.nextSibling.classList.contains("twitch-chat-buttons-hidden")) {
                deleteTag.classList.add("twitch-chat-buttons-hidden");
            } else {
                deleteTag.classList.remove("twitch-chat-buttons-hidden");
            }
        } else {
            deleteTag.classList.add("twitch-chat-buttons-hidden");
        }
    });
}

function twitchChatButtonsImportExportButtonInterface () {
    if (document.querySelector(".twitch-chat-button-add").classList.contains("twitch-chat-button-focused")) {
        twitchChatButtonsAddButtonInterface();
    }
    if (document.querySelector(".twitch-chat-button-del").classList.contains("twitch-chat-button-focused")) {
        twitchChatButtonsDelButtonInterface();
    }
    document.querySelector(".twitch-chat-buttons-div-settings-import-export").classList.toggle("twitch-chat-buttons-hidden");
    document.querySelector(".twitch-chat-button-import-export").classList.toggle("twitch-chat-button-focused");
}

window.addEventListener("load", () => {
    if (document.querySelector(".stream-chat").length !== 0) {
        if (!localStorage.buttons) {
            localStorage.buttons = JSON.stringify([]);
        }
        twitchChatButtonsCreateStyle();
        twitchChatButtonsCreateInterface();
        document.querySelectorAll(".no-wysiwyg").forEach((noWysiwygTag) => {
            noWysiwygTag.addEventListener("click", () => {
                wysiwygDestroyer();
            });
        });
        document.querySelector(".twitch-chat-button-icon").addEventListener("click", () => {
            twitchChatButtonsToggle();
        });
        document.querySelector(".twitch-chat-button-add").addEventListener("click", () => {
            twitchChatButtonsAddButtonInterface();
        });
        document.querySelector(".twitch-chat-button-del").addEventListener("click", () => {
            twitchChatButtonsDelButtonInterface();
        });
        document.querySelector(".twitch-chat-button-import-export").addEventListener("click", () => {
            twitchChatButtonsImportExportButtonInterface();
        });
        document.querySelector(".twitch-chat-button-field-sendable").addEventListener("click", () => {
            document.querySelector("[for='twitch-chat-button-field-sendable']").classList.toggle("twitch-chat-button-focused");
        });
        document.querySelector(".twitch-chat-button-add-button").addEventListener("click", () => {
            twitchChatButtonsAddButton();
        });
        document.querySelector(".twitch-chat-button-export-these").addEventListener("click", () => {
            downloadFile(JSON.stringify(JSON.parse(localStorage.buttons).filter((btn) => btn.user === getStreamerName())), `twitch-chat-buttons-${getStreamerName()}.json`);
        });
        document.querySelector(".twitch-chat-button-import-these").addEventListener("change", () => {
            uploadFile(document.querySelector("#twitch-chat-button-import-these"), (fileContent) => {
                twitchChatButtonsImportTheseButtons(fileContent);
            });
        });
        document.querySelector(".twitch-chat-button-export-all").addEventListener("click", () => {
            downloadFile(localStorage.buttons, "twitch-chat-buttons.json");
        });
        document.querySelector(".twitch-chat-button-import-all").addEventListener("change", () => {
            uploadFile(document.querySelector("#twitch-chat-button-import-all"), (fileContent) => {
                twitchChatButtonsImportAllButtons(fileContent);
            });
        });
    }
});
