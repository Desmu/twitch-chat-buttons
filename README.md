# Twitch Chat Buttons

Permet d'ajouter des boutons dans le chat Twitch pour spammer des messages prédéfinis.

<div align="center">
![Aperçu](twitch-chat-buttons.png)
</div>

## Installation

1. Télécharger [Greasemonkey pour Firefox](https://addons.mozilla.org/fr/firefox/addon/greasemonkey/) ou [Tampermonkey pour Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo), des plugins pour le navigateur permettant d'exécuter ses propres scripts sur une page web.
2. Ouvrir ensuite le script en cliquant sur ce lien : [`twitch-chat-buttons.user.js`](https://gitlab.com/Desmu/twitch-chat-buttons/-/raw/main/twitch-chat-buttons.user.js), le plugin devrait automatiquement proposer son installation.
3. L'interface permettant d'ajouter des boutons devrait apparaître sous le chat de tous les streams Twitch et dans leur version popup.

## Utilisation

- Cliquer sur le bouton `Boutons` à côté du bouton officiel `Chat` pour ouvrir l'interface générale.
- L'interface se divise en trois lignes, avec de bas en haut :
    * la ligne de menu, permettant d'ajouter, supprimer, importer ou exporter des boutons.
    * la ligne des boutons de groupes, cliquer sur un bouton de groupe affichera tous les boutons du groupe donné et masquera les autres.
    * la ligne des boutons envoyant les messages donnés.
- Chaque stream dispose de son propre set de boutons. Ajouter ou supprimer un bouton d'un stream ne l'ajoutera ni le supprimera d'un autre stream. Pour ajouter ou supprimer une même commande de plusieurs streams, il est nécessaire de répéter l'opération sur chaque stream concerné.
- Cliquer sur `Ajouter des boutons` pour ouvrir l'interface d'ajout, renseigner un titre qui apparaîtra sur le bouton pour l'identifier, le texte à envoyer (une commande de bot par exemple), éventuellement un nom de groupe dans lequel le placer (pour regrouper tous les boutons liés à une même fonctionnalité en une section), éventuellement décocher la case `Envoyer le texte au clic` pour préremplir le champ de message sans l'envoyer, puis cliquer sur `Ajouter un bouton` pour valider l'ajout et faire appraître le bouton sur l'interface. Recliquer ensuite sur `Ajouter des boutons` pour fermer l'interface d'ajout quand tous les boutons ont été ajoutés.
- Cliquer sur `Supprimer des boutons` pour ouvrir l'interface de suppression, cliquer sur le bouton `X` à côté d'un bouton de commande pour valider sa suppression et faire disparaître le bouton de l'interface. Recliquer ensuite sur `Supprimer des boutons` pour fermer l'interface de suppression quand tous les boutons concernés ont été supprimés.
- Cliquer sur `Importer/Exporter` pour ouvrir l'interface d'import/export de configurations, cliquer sur le bouton `Exporter les boutons de ce chat` pour récupérer tous les boutons crées pour un seul chat, ou sur `Importer les boutons d'un seul chat` pour remplacer tous les boutons d'un seul chat par les boutons définis par un fichier crée via le bouton précédent, ou sur `Exporter tous les boutons` pour exporter tous les boutons crées pour tous les chats, ou sur `Importer tous les boutons` pour remplacer tous les boutons par les boutons définis par un fichier crée via le bouton précédent.
- Si un bouton n'appartient à aucun groupe, il sera affiché si aucun bouton de groupe n'est sélectionné.

## NO WYSIWYG

Le champ de texte Twitch a été récemment mis à jour pour intégrer éditeur `What You See Is What You Get` (permettant aux emotes de s'afficher directement dans ce champ en lieu et place de leur identifiant textuel). Ce changement modifie en profondeur le fonctionnement de ce champ, rendant l'exécution de ce script moins fluide.
Si ce script devient inutilisable, il est néanmoins possible de désactiver ce nouveau type de champ de texte en cliquant sur le bouton `NO WYSIWYG`, qui remettra en place l'ancienne version du champ de texte.
Il s'agit d'une solution temporaire, l'idée étant de faire en sorte que ce nouveau type de champ de texte soit optimalement supporté à terme, en partant du principe qu'il ne sera un jour potentiellement plus possible de rebasculer sur l'ancienne version du champ. C'est pourquoi le changement de type de champ n'est conservé que le temps d'une session pour prévenir un éventuel problème : le nouveau type de champ de texte reviendra au rechargement de la page (et il redevient alors possible de le désactiver).